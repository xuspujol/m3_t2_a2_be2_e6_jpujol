# M3_T2_A2_Be2_E6_jpujol



# Index
- ### [L'Exercici 1](#exercici-1)
- ### [L'Exercici 2](#exercici-2)
- ### [L'Exercici 3](#exercici-3)
- ### [L'Exercici 4](#exercici-4)
- ### [L'Exercici 5](#exercici-5)
- ### [L'Exercici 6](#exercici-6)
- ### [L'Exercici 7](#exercici-7)
- ### [L'Exercici 8](#exercici-8)
- ### [L'Exercici 9](#exercici-9)
- ### [L'Exercici 10](#exercici-10)
- ### [L'Exercici 11](#exercici-11)
- ### [L'Exercici 12](#exercici-12)
- ### [L'Exercici 13](#exercici-13)
- ### [L'Exercici 14](#exercici-14)
- ### [L'Exercici 15](#exercici-15)
- ### [L'Exercici 16](#exercici-16)
- ### [L'Exercici 17](#exercici-17)
- ### [L'Exercici 18](#exercici-18)
- ### [L'Exercici 19](#exercici-19)
- ### [L'Exercici 20](#exercici-20)
- ### [L'Exercici 21](#exercici-21)


<br><br>


## **Exercici 1:**
**Justifica la utilització de les estructures iteratives que coneixes i posa’n exemples.** 
<br>

**Definicio:**

Indica un conjunt d'instruccions que cal repetir mentre que la resposta a l'expressió que es col·loca dins del símbol de decisió sigui VERDAREA, per tant quan la resposta a la condició sigui FALSA es continua amb la següent instrucció és a dir surt del cicle.

**Mentre:(While)**

En aquesta iteracio col.loquem una variable.
Li diem que fins que no sigui mes gran o igual a el numero dins de l'instruccio(100) , que es vagi repetint el cicle i finalment pujem un punt a variable per no entrar en bucle i que pugui acabar d'executar el programa

<br>

```c
int variable;

variable=0

**while(variable<=100){ 
    prinf("%i ,",variable);
    variable++<br>
}

```


<br><br>

[Tornar a l'index...](#index)

## **Exercici 2:** 
**Disseny d’un algorisme que ens demani l’edat 100 vegades.**<br><br>

```c
#define MAX 100

int main()
{
    int edat, comptador;
    //Inicialitzador<
    comptador=1;
    while(comptador<=MAX ){
    //tractar
    printf("Introdueix edat: ");
    scanf("%i" ,&edat);
    //obtenir següent 
    comptador++;
    }
```
<br>

![](Imatges/Exercici2.png)


<br><br>


[Tornar a l'index...](#index)
<br><br>

## **Exercici 3:** 
**Disseny d’un algorisme que mostri els 100 primers números naturals** <br><br>


```c
#define MAX 100

int main()
{

int comptador;

comptador=1;
while(comptador<=MAX){

    printf("%i, " ,comptador);

    comptador++;
}

```
<br>

![](Imatges/Exercici3.png)



[Tornar a l'index...](#index)
<br><br><br>

## **Exercici 4:** 
**Disseny d’un algorisme que mostri els nombres parells de l’1 al 100.**
<br><br>

```c

#define MAX 100 

int main() 
{ 
    int comptador;

    comptador=2; 
    while(comptador<=MAX){
        printf("%i, ",comptador);
        comptador=comptador+2;

    }

```

<br><br>


![](Imatges/Exercici4.png)


<br>
[Tornar a l'index...](#index)
<br><br>


## **Exercici 5:** 
**Disseny d’un algorisme que mostri els nombres senars de l’1 al 100.**
<br><br>

```c
#define MAX 100

int main()<br>
{
    int comptador;<br>

    comptador=1;
    while(comptador<=MAX){
        printf("%i, ",comptador);
        comptador=comptador+2;

    }

```
<br>

![](Imatges/Exercici5.png)

<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 6:** 
**Disseny d’un algorisme que mostri els nombres primers que hi ha entre l’1 i el 100 inclosos.**

```c
#include <stdio.h>
#include <stdlib.h>
#define MAX 100


int main()
{
    int elements[MAX];
    int i,j;
    //Inicialitzem el vector
    for(i=1;i<MAX;i++) elements[i]=i;
    //Primer recorregut fins a MAX/2
    for (i=2;i<MAX/2;i++){
        //Segon recorregut per cada element del primer recorregut
        for(j=i+i;j<MAX;j=j+i){

        elements[j]=0;
        }
    }
    //Mostrar resultat si si no es 0...
    for(i=1;i<MAX;i++) if(elements[i]!=0) printf("%i, ",elements[i]);



    return 0;
}

```

![](Imatges/Exercici6.png)





<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 7:** 
**Disseny d’un algorisme que compti de 5 en 5 fins a 100.**

```c
#define MAX 100

int main()
{
    int comptador;

    comptador=0;
    while(comptador<=MAX){
        printf("%i, ",comptador);
        comptador=comptador+5;

    }

```
<br>

![](Imatges/Exercici7.png)


<br><br>
[Tornar a l'index...](#index)
<br>

## **Exercici 8:** 
**Disseny d’un algorisme que vagi del número 100 al número 1 de forma Descendent.** 

```c
#define MAX 100
#define MIN 1

int main()
{
    int comptador;

    comptador=100;
    while(comptador>=MIN){
        printf("%i, ",comptador);
        comptador=comptador-1;

    }

```
<br> 

![](Imatges/Exercici8.png)

<br> <br>

[Tornar a l'index...](#index)

<br>

## **Exercici 9:** 
**Disseny d’un algorisme que mostri “n” termes de la successió de Fibonacci. Els dos primers termes valen 1 i el terme n-èssim és la suma dels dos anteriors. És a dir, an = an-1 + an-2 .** 

```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{


int comptador ,ntermes;
int n;
int n1;
int n2;

    ntermes=0;
    n1=0;
    n2=1;
    n=0;

    printf("Quants termes de la successio de Fibonacci vols que calculi? ");
    scanf("%i," ,&ntermes);


      if(ntermes>=2) printf("1, ");

        while(comptador<ntermes){

        //Tractar calcular un terme de la succesio de FIBONACC:

            n=n1+n2;
            n1=n2;
            n2=n;

            printf("%i, ",n);

            //obtenir següent
            comptador++;

}


return 0;

}

```
<br>

![](Imatges/Exercici9.png)


<br> <br>

[Tornar a l'index...](#index)

<br><br>


## **Exercici 10:** 
**A partir de la següent especificació: { anys=ANYS ^ dies=DIES ^ ^hores=HORES ^ minuts= MINUTS ^ segons=SEGONS ^ anys>0 ^ ^0<=dies<365 ^ 0<=hores<24 ^0<=minuts<60 ^ 0<=segons<60 }. Dissenyeu un algorisme que incrementi el temps en un segon.** 


```c
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100

int main()
{

int segons,minuts,hores,dies,any;

segons=0;
minuts=0;
hores=0;
dies=0;
any=0;

while(1){

    segons++;

    if(segons==60){
        segons=0;
        minuts++;}
        if(minuts==60){
            minuts=0;
            hores++;}
            if(hores==24){
                hores=0;
                dies++;}
                if(dies==365){
                    dies=0;
                    any++;}



        system("cls");
        printf("Any: %.2i, Dies: %.2i, Hores: %.2i, Minuts: %.2i, Segons: %.2i",any,dies,hores,minuts,segons);
        Sleep(1000);

}

return 0;


}
```

<br>

![](Imatges/Exercici10.png)


<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 11:** 
**Disseny d’un algorisme que calculi les solucions d’una equació de 2n. grau.** 

```c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define BB while(getchar()!='\n');

int main(){

    int a,b,c,X1,X2,arrel;

    a=0;
    b=0;
    c=0;

    printf("Introdueix el valor a: ");
    scanf("%i[^\n]",&a);BB;

    printf("Introdueix el valor b: ");
    scanf("%i[^\n]",&b);BB;

    printf("Introdueix el valor c: ");
    scanf("%i[^\n]",&c);BB;


    arrel=(pow(b,2)-4*a*c);
    X1= (-b + arrel)/(2*a);
    X2= (-b - arrel)/(2*a);

    printf("Les solucions son: %i i %i",X1,X2);


    return 0;

}

```

<br>

![](Imatges/Exercici11.png)



<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 12:** 
**Donat un text calcular la seva longitud. Recorda que un text, en C, acaba amb el caràcter de final de cadena ‘\0’. A partir d’ara “text” i/o “paraula” ho llegirem com cadena de caràcters o string.** 

```c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX 100
#define BB while(getchar()!='\n');

int main(){

char text[MAX+1];
int comptador;

    comptador=0;

    printf("Introdueix el text: ");
    scanf("%100[^\n]",text);BB;

    while(text[comptador]!='\0'){

        comptador++;

    }

        printf("Aquest es el nombre de lletres: %i" ,comptador);

return 0;
}

```

<br>

![](Imatges/Exercici12.png)

<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 13:** 
**Donat un text comptar el nombre de vocals.** 

```c

#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n');
#define MAX 25
#define MIN 5

int main()
{


int comptador,index;
char vocals[MIN],text[MAX];



    vocals[0]='a';
    vocals[1]='e';
    vocals[2]='i';
    vocals[3]='o';
    vocals[4]='u';


    comptador=0;
    index=0;


    printf("Introdueix el teu text: ");
    scanf("%25[^\n]",text);BB;


        while(text[index]!='\0'){
            if(text[index]==vocals[0]||
            text[index]==vocals[1]||
            text[index]==vocals[2]||
            text[index]==vocals[3]||
            text[index]==vocals[4]){


            comptador++;

            }
            index++;

            }

    printf("La teva paraula te %i vocals",comptador);


    return 0;
}

```
<br>

![](Imatges/Exercici13.png)



<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 14:** 
**Donat un text comptar el nombre de consonants.** 

```c

#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n');
#define MAX 25
#define MIN 21

int main()
{


int comptador,index;
char consonants[MIN],text[MAX];



    consonants[0]='q';
    consonants[1]='w';
    consonants[2]='r';
    consonants[3]='t';
    consonants[4]='y';
    consonants[5]='p';
    consonants[6]='s';
    consonants[7]='d';
    consonants[8]='f';
    consonants[9]='g';
    consonants[10]='h';
    consonants[11]='j';
    consonants[12]='k';
    consonants[13]='l';
    consonants[14]='z';
    consonants[15]='x';
    consonants[16]='c';
    consonants[17]='v';
    consonants[18]='b';
    consonants[19]='n';
    consonants[20]='m';

    comptador=0;
    index=0;


    printf("Introdueix el teu text: ");
    scanf("%25[^\n]",text);BB;


        while(text[index]!='\0'){
            if(text[index]==consonants[0]||
            text[index]==consonants[1]||
            text[index]==consonants[2]||
            text[index]==consonants[3]||
            text[index]==consonants[4]||
            text[index]==consonants[5]||
            text[index]==consonants[6]||
            text[index]==consonants[7]||
            text[index]==consonants[8]||
            text[index]==consonants[9]||
            text[index]==consonants[10]||
            text[index]==consonants[11]||
            text[index]==consonants[12]||
            text[index]==consonants[13]||
            text[index]==consonants[14]||
            text[index]==consonants[15]||
            text[index]==consonants[16]||
            text[index]==consonants[17]||
            text[index]==consonants[18]||
            text[index]==consonants[19]||
            text[index]==consonants[20]){




            comptador++;

            }
            index++;

            }

    printf("La teva paraula te %i consonants",comptador);

    return 0;
}

```

<br>

![](Imatges/Exercici14.png)


<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 15:** 
**Donat un text capgirar-lo.** 

```c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX 100

int main()
{

 char text[MAX],index,c;


    printf("Introdueix un text: ");
    scanf("%100[^\n]",text);

    index=0;

    while(text[index++]!= '\0');

    printf("%c Escrita al reves: ",text);


    while(index>=0)
        printf("%c",text[index--]);


     return 0;
}


```

<br>

![](Imatges/Exercici15.png)



<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 16:** 
**Donat un text comptar el nombre de paraules que acaben en “ts”.** 

```c

#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n');
#define MAX 25
#define MIN 2

int main()
{


int comptador,index;
char terminacio[MIN],text[MAX];



    terminacio[0]='t';
    terminacio[1]='s';



        printf("Introdueix el teu text: ");
        scanf("%25[^\n]",text);BB;

         comptador=0;
         index=0;


        while(text[index]!='\0'){
            while(text[index]==' ') index++;
            while(text[index]!=' ' && text[index]!='\0')index++;

            if(text[index-1]=='s' && text[index-2]=='t'){
                comptador++;

            }
            index++;
        }


    printf("El teu text te %i ts",comptador);


    return 0;


}

```

<br>

![](Imatges/Exercici16.png)



<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 17:** 
**Donat un text comptar el nombre de paraules.** 

```c

#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100
#define BB while(getchar()!='\n')

int main()
{


 int index, comptador;
 char text[MAX_TEXT+1];


    printf("Introdueix un text: ");
        scanf("%100[^\n]",text);BB;

            index=0;
            comptador=0;



       while(text[index]!='\0'){
       while(text[index]==' ') index++;


       while(text[index]!='\0' && text[index]!=' ')index++;
            comptador++;

        }





    printf("Hi han %i paraules en aquest text",comptador);

     return 0;

```

<br>

![](Imatges/Exercici17.png)
     

<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 18:** 
**Donat un text dissenyeu un algorisme que compti els cops de apareixen conjuntament la parella de caràcters “as” dins del text.** 

```c

#include <stdio.h>
#include <stdlib.h>
#define BB while(getchar()!='\n');
#define MAX 25
#define MIN 2

int main()
{


int comptador,index;
char terminacio[MIN],text[MAX];



    terminacio[0]='a';
    terminacio[1]='s';



        printf("Introdueix el teu text: ");
        scanf("%25[^\n]",text);BB;

         comptador=0;
         index=0;


        while(text[index]!='\0'){
            while(text[index]==' ') index++;
           if(text[index]=='a' && text[index+1]=='s'){
                comptador++;

            }
            index++;
        }


    printf("La teva paraula te %i as",comptador);


    return 0;


}

```

<br>

![](Imatges/Exercici18.png)


<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 19:** 
**Donat un text i una paraula (anomenada parbus). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text. Nota: parbus és el nom de la variable que conté la paraula a cercar.** 

```c

#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100
#define MAX_PARAULA 100
#define MAX_BUSCA 100
#define BB while(getchar()!='\n')

int main()
{


 int index, index2;
 char text[MAX_TEXT+1], paraula[MAX_PARAULA+1], buscador[MAX_BUSCA+1];


    printf("Introdueix un text: ");
        scanf("%100[^\n]",text);BB;
    printf("Introdueix la paraula que vols buscar: ");
        scanf("%100[^\n]",paraula);BB;

            index=0;



        while(text[index]!='\0'){
        while(text[index]==' ') index++;

            index2=0;
            
            
        while(text[index]!='\0' && (text[index]!=' ')){
            buscador[index2]=text[index];


            index++;
            index2++;
        }


            buscador[index2]='\0';

            index2=0;


     while(paraula[index2]!='\0' && buscador[index2]!='\0'){
        if(paraula[index2]!=buscador[index2])break;
        
        
        index2++;
        
        
     }
        if(paraula[index2]=='\0' && buscador[index2]=='\0'){
            printf("S'ha trobat la paraula");
            break;
        }

    }

     return 0;
}

```

<br>


![](Imatges/Exercici19.png)


<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 20:** 
**Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text** 

```c

#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100
#define MAX_PARAULA 100
#define MAX_BUSCADOR 100
#define BB while(getchar()!='\n')

int main()
{


 int index, index2, comptador;
 char text[MAX_TEXT+1], paraula[MAX_PARAULA+1],buscador[MAX_BUSCADOR+1];


    printf("Introdueix un text: ");
        scanf("%100[^\n]",text);BB;
    printf("Introdueix la paraula que vols buscar: ");
        scanf("%100[^\n]",paraula);BB;

            index=0;
            comptador=0;

    //SALTAR BLANC
        while(text[index]!='\0'){
        while(text[index]==' ') index++;

        index2=0;

        //Indexar paraula introduida i paraula que busquem (copiar)
        while(text[index]!='\0' && (text[index]!=' ')){
            buscador[index2]=text[index];
            index++;
            index2++;

        }
        buscador[index2]='\0';

       index2=0;

        //Comparacio de les paraules del text i el buscador
        while(paraula[index2]!='\0' && buscador[index2]!='\0'){
            if(paraula[index2]!=buscador[index2])break;


        index2++;
        
        //Si les dos paraules acaben al mateix moment, augmenta en 1 a comptador
     }
        if(paraula[index2]=='\0' && buscador[index2]=='\0'){
            comptador++;

        }

    }
    printf("Hi han %i cops la paraula repetida",comptador);

     return 0;
}

```
<br>

![](Imatges/Exercici20.png)


<br> <br>

[Tornar a l'index...](#index)

<br><br>

## **Exercici 21:** 
**Donat un text i una paraula (parbus) i una paraula (parsub). Dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).** 

```c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX_TEXT 100
#define MAX_PARBUS 100
#define MAX_PARSUB 100
#define MAX_TMP 100
#define BB while(getchar()!='\n');

int main()
{

    char text[MAX_TEXT+1],text2[MAX_TEXT+1],parbus[MAX_PARBUS+1],partemp[MAX_TMP+1],parsub[MAX_PARSUB+1];
    int it, it2,ib,is,itmp;

    //inicialtitzacio
    printf("Introduix el teu text: ");
    scanf("%100[^\n]",text);BB;

    printf("Introdueix la paraula a buscar: ");
    scanf("%100[^\n]",parbus);BB;

    printf("Introdueix la paraula a substituir: ");
    scanf("%100[^\n]",parsub);BB;

    it=0;
    it2=0;

    //iteració principla mentre final de text
    while(text[it]!='\0'){
        //Saltar blanc i copiar-los a text2
        while(text[it]==' '){
            text2[it2]=text[it];
            it++;
            it2++;
        }

        //Copiar text a temporal
        itmp=0;
        while(text[it]!=' ' && text[it]!='\0'){
            partemp[itmp]=text[it];
            it++;
            itmp++;
        }
        partemp[itmp]='\0';

        //Són iguals(partemp,parbus)?
        ib=0;

        while(partemp[ib]!='\0' || parbus[ib]!='\0'){
            if(partemp[ib]!=parbus[ib]) break;
            ib++;
        }
        //partemp[ib]=='\0' && parbus[ib]=='\0'
        //partemp[ib]!=parbus[ib]
        //Copiar de temporal a text2 en cas de que coincideixi

        is=0;
        //si son iguals(partemp,parbus)...
        if(partemp[ib]=='\0' && parbus[ib]=='\0'){
           while (parsub[is]!='\0'){
                  text2[it2]=parsub[is];
                  it2++;
                  is++;
            }
        }else{
            while(partemp[is]!='\0'){
                text2[it2]=partemp[is];
                it2++;
                is++;
            }
        }

    text2[it2]='\0';

}

printf("El resultat es: %s",text2);




return 0;

}

```
<br>

![](Imatges/Exercici21.png)


<br> <br>

[Tornar a l'index...](#index)

<br><br>
